﻿using UnityEngine;
using System.Collections;

public class Despawn : MonoBehaviour 
{
    NetworkView networkView;
    public float despawnTime;
    float timer;

	void Start () 
    {
        networkView = GetComponent<NetworkView>();
        timer = despawnTime;
	}

    void FixedUpdate ()
    {
        timer -= Time.deltaTime;
        if (timer <= 0.0f)
        {
            if (networkView != null)
            networkView.RPC("Destroy", RPCMode.AllBuffered);
        }
    }

    [RPC]
    public void Destroy()
    {
        Destroy(gameObject);
    }
}
