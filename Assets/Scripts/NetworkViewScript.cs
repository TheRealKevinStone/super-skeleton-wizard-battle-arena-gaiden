﻿using UnityEngine;
using System.Collections;

public class NetworkViewScript : MonoBehaviour 
{
    NetworkView networkView;

	// Use this for initialization
	void Start () 
    {
        networkView = GetComponent<NetworkView>();

        if (!networkView.isMine)
        {
            Component.Destroy(gameObject);
        }
	}
}
