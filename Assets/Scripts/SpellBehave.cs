﻿using UnityEngine;
using System.Collections;

public class SpellBehave : MonoBehaviour 
{
    public float initialForce;
    public float force;
    public float lifeSpan;

	// Use this for initialization
	void Start () 
    {
        GetComponent<Rigidbody>().AddRelativeForce(initialForce, 0, 0);
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate ()
    {
        lifeSpan -= Time.deltaTime;
        if (lifeSpan <= 0)
            Destroy(gameObject);
        GetComponent<Rigidbody>().AddRelativeForce(force * Time.deltaTime, 0, 0);
    }

    void OnCollisionEnter (Collision c)
    {
        if (c.gameObject.tag == "Enemy")
        {
            EnemyAI eHealth = c.gameObject.GetComponent<EnemyAI>();
            if (eHealth != null)
            {
                eHealth.GetRekt();
            }
        }
        if (c.gameObject.tag == "Player")
        {
            PlayerControl pHealth = c.gameObject.GetComponent<PlayerControl>();
            if (pHealth != null)
            {
                pHealth.GetHit();
            }
        }
        Destroy(gameObject);
    }
}
