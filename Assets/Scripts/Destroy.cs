﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour 
{
    NetworkView networkView;

	// Use this for initialization
	void Start () 
    {
        networkView = GetComponent<NetworkView>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void Despawn()
    {
        networkView.RPC("DestroyMe", RPCMode.AllBuffered);
    }

    [RPC]
    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
