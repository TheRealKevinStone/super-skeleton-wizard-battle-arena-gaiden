﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour 
{
    public float bombTimer;
    public float force;
    public float explosionRadius;
    public GameObject explosion;

	// Use this for initialization
	void Start () 
    {
        GetComponent<Rigidbody>().AddRelativeForce(force, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate ()
    {
        bombTimer -= Time.deltaTime;
        if (bombTimer <= 0)
        {
            Explode();
        }
    }

    void OnCollisionEnter (Collision c)
    {
        Explode();
    }

    void Explode()
    {
        Instantiate(explosion, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider c in colliders)
        {
            EnemyAI eHealth = c.gameObject.GetComponent<EnemyAI>();
            PlayerControl pHealth = c.gameObject.GetComponent<PlayerControl>();

            if (eHealth != null)
            {
                eHealth.GetRekt();
            }
            if (pHealth != null)
            {
                pHealth.GetHit();
            }
        }
        Destroy(gameObject);
    }
}
