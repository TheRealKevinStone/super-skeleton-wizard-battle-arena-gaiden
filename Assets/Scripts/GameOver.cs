﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour
{
    public void PlayAgain ()
    {
        Application.LoadLevel("Level");
    }

    public void MainMenu ()
    {
        Application.LoadLevel("MainMenu");
    }
}
