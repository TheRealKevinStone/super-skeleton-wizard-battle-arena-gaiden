﻿using UnityEngine;
using System.Collections;

public class FreezeSpellEffect : MonoBehaviour 
{
    public float explosionRadius;
    public float duration;

	// Use this for initialization
	void Start () 
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider c in colliders)
        {
            EnemyAI eHealth = c.gameObject.GetComponent<EnemyAI>();
            //PlayerControl pHealth = c.gameObject.GetComponent<PlayerControl>();

            if (eHealth != null)
            {
                eHealth.Frozen(duration);
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
