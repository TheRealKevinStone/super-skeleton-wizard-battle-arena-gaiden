﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour 
{
    //GUI Stuff
    public GameObject timeGUI;
    public GameObject heartGUI;
    GameObject gameOverUI;
    Text timeDisplay;
    Text heartDisplay;
    float timeClock;

    // Player Stats
    public float heartCounter;
    CharacterController player;
    bool isHurt;
    public float knockBackDistance;
    public float knockBackDuration;
    float knockBackDurationTimer;
    NetworkView networkView;

    // Animation Variables
    private Animator anim;
    private AnimatorStateInfo currentBaseState;
    private AnimatorStateInfo layer2CurrentState;

    static int reloadState = Animator.StringToHash("Layer2.Reload");
    
    // Movement Variables
    public float speed = 3.0f;
    float speedX;
    Vector3 moveSpeed;
    bool facingRight;

    // Jump Variables
    public float jumpSpeed = 2.0f;
    public float jumpSpeedTimer = 0.25f;
    public float jumpTimer;
    float speedY;
    bool canDoubleJump;

    // Attack Variables
    public Transform castPoint;
    public GameObject spell;
    public float spellCooldown;
    float spellCooldownTimer;
    public float castTime;
    float castTimeTimer;
    bool canCast;

    // Bomb Variables
    public GameObject fireGUI;
    public bool hasBomb;
    public GameObject bombPrefab;
    public Transform spawnBomb;
    public float bombCooldown;
    public float bombCost;
    float bombCooldownTimer;
    bool canCastBomb;

    // Cape Variables
    public GameObject airGUI;
    public GameObject cape;
    public float capeCost;
    public bool hasCape;
    public float capeSpeed = 4.0f;
    public float cachedGravity;
    
    // Freeze Variables
    public GameObject iceGUI;
    public bool hasFreeze;
    public float freezeDuration;
    public float freezeCooldown;
    public float freezeCost;
    float freezeCooldownTimer;
    bool canCastFreeze;
    public GameObject freezeSpellEffect;
    

	// Use this for initialization
	void Start () 
    {
        networkView = GetComponent<NetworkView>();

        if (!networkView.isMine)
        {
            Component.Destroy(this);
        }

        // Find HUD objects
        iceGUI = GameObject.FindWithTag("IceGUI");
        airGUI = GameObject.FindWithTag("AirGUI");
        fireGUI = GameObject.FindWithTag("FireGUI");
        timeGUI = GameObject.FindWithTag("Timer");
        heartGUI = GameObject.FindWithTag("HeartCounter");

        // Set Spell Icons to inactive
        iceGUI.SetActive(false);
        airGUI.SetActive(false);
        fireGUI.SetActive(false);

        Time.timeScale = 1.0f;
        timeClock = 0.0f;
        knockBackDistance = knockBackDistance / knockBackDuration;
        isHurt = false;
        cachedGravity = Physics.gravity.y;
        player = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        anim.speed = 1.0f;
        if (anim.layerCount == 2)
            anim.SetLayerWeight(1, 1);
        spellCooldownTimer = 0.0f;
        bombCooldownTimer = bombCooldown;
        freezeCooldownTimer = freezeCooldown;
        gameOverUI = GameObject.FindWithTag("GameOver");
        gameOverUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        bombCooldownTimer -= Time.deltaTime;
        //// Animation Stuff
        //float h = Input.GetAxis("Horizontal");
        //anim.SetFloat("Speed", h);
        //currentBaseState = anim.GetCurrentAnimatorStateInfo(0);

        //if (h > 0.1)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        //}
        //else if (h < -0.1)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        //}
        //if (!player.isGrounded)
        //{
        //    anim.SetBool("Jump", true);
        //}
        //else
        //{
        //    anim.SetBool("Jump", false);
        //}

        //if (anim.layerCount == 2)
        //{
        //    layer2CurrentState = anim.GetCurrentAnimatorStateInfo(1);
        //}
        //if (Input.GetButtonDown("Fire1") && spellCooldown <= 0)
        //{
        //    anim.SetTrigger("Attack");
        //}
        if (isHurt)
        {
            // Do Hurt Stuff
            anim.SetBool("Hit", true);
            knockBackDurationTimer -= Time.deltaTime;
            
            moveSpeed = new Vector3(-knockBackDistance * Time.deltaTime, 0.0f, 0.0f);
            moveSpeed = transform.rotation * moveSpeed;
            player.Move(moveSpeed);
            if (knockBackDurationTimer <= 0)
            {
                isHurt = false;
                anim.SetBool("Hit", false);
            }
        }
        else
        {
            // Let Player Act Normally

            // Move Animation
            float h = Input.GetAxis("Horizontal");
            anim.SetFloat("Speed", h);
            currentBaseState = anim.GetCurrentAnimatorStateInfo(0);

            if (h > 0.1)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
            else if (h < -0.1)
            {
                transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
            if (!player.isGrounded)
            {
                anim.SetBool("Jump", true);
            }
            else
            {
                anim.SetBool("Jump", false);
            }

            if (anim.layerCount == 2)
            {
                layer2CurrentState = anim.GetCurrentAnimatorStateInfo(1);
            }

            // Attack
            castTimeTimer -= Time.deltaTime;
            spellCooldownTimer -= Time.deltaTime;
            if (Input.GetButton("Fire1") && spellCooldownTimer <= 0)
            {
                spellCooldownTimer = spellCooldown;
                castTimeTimer = castTime;
                canCast = true;
                anim.SetTrigger("Attack");
            }
            if (castTimeTimer <= 0 && canCast)
            {
                Instantiate(spell, castPoint.position, transform.rotation);
                canCast = false;
            }

            // Special Items
            // Cape
            if (hasCape && Input.GetButton("Jump") && !player.isGrounded && speedY <= 0 && heartCounter > 0.0f)
            {
                //Physics.gravity = new Vector3(0f, capeSpeed, 0f);
                heartCounter -= Time.deltaTime * 0.5f;
                speedY = capeSpeed * Time.deltaTime;
            }
            else
            {
                Physics.gravity = new Vector3(Physics.gravity.x, cachedGravity, Physics.gravity.z);
            }

            // Bomb
            if (hasBomb)
            {
                bombCooldownTimer -= Time.deltaTime;
                if (Input.GetButtonDown ("Fire2") && bombCooldownTimer <= 0 && heartCounter >= bombCost)
                {
                    heartCounter -= bombCost;
                    anim.SetTrigger("Attack");
                    bombCooldownTimer = bombCooldown;

                    castTimeTimer = castTime;
                    canCastBomb = true;
                }
                if (castTimeTimer <= 0 && canCastBomb)
                {
                    Instantiate(bombPrefab, spawnBomb.position, spawnBomb.rotation);
                    canCastBomb = false;
                    castTimeTimer = castTime;
                }
            }

            // Freeze
            if (hasFreeze)
            {
                //GameObject[] enemy = GameObject.FindGameObjectsWithTag("Enemy");
                //EnemyAI[] eFrozen = new EnemyAI[enemy.Length];
                //Debug.Log(enemy.Length);

                //for (int i = 0; i < enemy.Length; i++)
                //{
                //    Debug.Log(i);
                //    if (enemy != null && eFrozen != null)
                //    {
                //        eFrozen[i] = enemy[i].GetComponent<EnemyAI>();
                //        //eFrozen[i].GetComponent<EnemyAI>().enabled = true;
                //    }
                //}
                //for (int i = 0; 1 < eFrozen.Length; i++)
                //{
                //    if (eFrozen != null)
                //    {
                //        eFrozen[i].Frozen(freezeDuration);
                //    }
                //}
                freezeCooldownTimer -= Time.deltaTime;
                if (Input.GetButtonDown("Fire2") && freezeCooldownTimer <= 0 && heartCounter >= freezeCost)
                {
                    heartCounter -= freezeCost;
                    anim.SetTrigger("Attack");
                    freezeCooldownTimer = freezeCooldown;

                    castTimeTimer = castTime;
                    canCastFreeze = true;
                }
                if (castTimeTimer <= 0 && canCastFreeze)
                {
                    Instantiate(freezeSpellEffect, transform.position, transform.rotation);
                    canCastFreeze = false;
                    castTimeTimer = castTime;
                }
            }

            // Move left and right
            speedX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

            // Jump
            if (player.isGrounded && Input.GetButtonDown("Jump"))
            {
                jumpTimer = 0.0f;
                speedY = jumpSpeed * Time.deltaTime;
            }
            else if (Input.GetButtonDown("Jump") && canDoubleJump)
            {
                jumpTimer = 0.0f;
                speedY = jumpSpeed * Time.deltaTime;
                canDoubleJump = false;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                jumpTimer = jumpSpeedTimer;
            }
            else if (Input.GetButton("Jump") && jumpTimer < jumpSpeedTimer)
            {
                jumpTimer += Time.deltaTime;
                speedY = jumpSpeed * Time.deltaTime;
            }
            else if (player.isGrounded)
            {
                canDoubleJump = true;
                speedY = -0.01f;
            }
            else
            {
                speedY += Physics.gravity.y * Time.deltaTime * Time.deltaTime;
            }

            //Calculate final movement
            moveSpeed = new Vector3(speedX, speedY, 0.0f);
            player.Move(moveSpeed);
        }
	}

    void FixedUpdate ()
    {
        if (heartCounter < 0)
        {
            heartCounter = 0;
        }
        timeClock += Time.deltaTime;
        timeDisplay = timeGUI.GetComponent<Text>();
        timeDisplay.text = "" + timeClock;
        heartDisplay = heartGUI.GetComponent<Text>();
        heartDisplay.text = "Hearts: " + (int)heartCounter;
        
    }

    void OnTriggerEnter (Collider col)
    {
        if (col.gameObject.tag == "Death")
        {
            gameOverUI.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Floor")
        {
            transform.parent = col.transform;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Floor")
        {
            transform.parent = null;
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Enemy")
        {
			//Debug.Log(hit.gameObject);
            isHurt = true;
            knockBackDurationTimer = knockBackDuration;
            //Debug.Log("Ouch!");
        }
        if (hit.gameObject.tag == "Fireball")
        {
            hasBomb = true;
            hasFreeze = false;
            hasCape = false;
            cape.SetActive(false);
            iceGUI.SetActive(false);
            fireGUI.SetActive(true);
            airGUI.SetActive(false);
            Destroy obj = hit.gameObject.GetComponent<Destroy>();
            if(obj != null)
            {
                obj.Despawn();
            }
        }
        if (hit.gameObject.tag == "Air")
        {
            hasBomb = false;
            hasCape = true;
            hasFreeze = false;
            iceGUI.SetActive(false);
            fireGUI.SetActive(false);
            airGUI.SetActive(true);
            cape.SetActive(true);
            Destroy obj = hit.gameObject.GetComponent<Destroy>();
            if (obj != null)
            {
                obj.Despawn();
            }
        }
        if (hit.gameObject.tag == "Freeze")
        {
            hasBomb = false;
            hasFreeze = true;
            hasCape = false;
            cape.SetActive(false);
            iceGUI.SetActive(true);
            fireGUI.SetActive(false);
            airGUI.SetActive(false);
            Destroy obj = hit.gameObject.GetComponent<Destroy>();
            if (obj != null)
            {
                obj.Despawn();
            }
        }
        if (hit.gameObject.tag == "Heart")
        {
            heartCounter += 1.0f;
            Destroy obj = hit.gameObject.GetComponent<Destroy>();
            if (obj != null)
            {
                obj.Despawn();
            }
        }
    }

    public void GetHit ()
    {
        isHurt = true;
        knockBackDurationTimer = knockBackDuration;
        //Debug.Log("Ouch!");
    }
}
