﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour 
{
    public float speed;
    private Vector3 velocity;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate ()
    {
        velocity = new Vector3(0f, speed * Time.deltaTime, 0f);
        transform.Translate(velocity);
    }
}
