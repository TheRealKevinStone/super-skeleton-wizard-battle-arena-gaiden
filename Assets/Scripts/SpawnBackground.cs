﻿using UnityEngine;
using System.Collections;

public class SpawnBackground : MonoBehaviour 
{
    public GameObject background;
    public float spawnTimer;
    float spawnTimerCD;

	// Use this for initialization
	void Start ()
    {
        spawnTimerCD = 0.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
    
    void FixedUpdate ()
    {
        spawnTimerCD -= Time.deltaTime;

        if (spawnTimerCD <= 0)
        {
            Instantiate(background, transform.position, Quaternion.Euler(270f, 0f, 0f));
            spawnTimerCD = spawnTimer;
        }
        //spawnTimer -= camMath.acceleration * Time.deltaTime * Time.deltaTime;
    }
}
