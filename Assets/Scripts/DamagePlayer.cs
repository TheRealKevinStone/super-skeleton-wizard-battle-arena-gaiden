﻿using UnityEngine;
using System.Collections;

public class DamagePlayer : MonoBehaviour 
{
    public float force;
    public float lifespan;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Rigidbody>().AddRelativeForce(force, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate()
    {
        lifespan -= Time.deltaTime;
        if (lifespan <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter (Collision c)
    {
        if (c.gameObject.tag == "Player")
        {
            PlayerControl pHealth = c.gameObject.GetComponent<PlayerControl>();
            pHealth.GetHit();
            Destroy(gameObject);
        }
    }
}
