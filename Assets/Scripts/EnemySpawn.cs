﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour 
{
    public GameObject enemy;

	// Use this for initialization
	void Start () 
    {
        int rand = Random.Range(1, 5);
        if (rand < 2)
        {
            enemy.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
