﻿using UnityEngine;
using System.Collections;

public class SpawnFloor : MonoBehaviour 
{
    public GameObject floor;
    public GameObject[] spawnPoints;
    public float spawnTimer;
    float spawnTimerCD;

	// Use this for initialization
	void Start () 
    {
        spawnTimerCD = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate ()
    {
        spawnTimerCD -= Time.deltaTime;

        if (spawnTimerCD <= 0.0f)
        {
            int rand = Random.Range(0, spawnPoints.Length);
            Instantiate(floor, spawnPoints[rand].transform.position, new Quaternion());
            int rand2 = Random.Range(0, spawnPoints.Length);
            while (rand2 == rand)
            {
                rand2 = Random.Range(0, spawnPoints.Length);
            }
            Instantiate(floor, spawnPoints[rand2].transform.position, new Quaternion());
            spawnTimerCD = spawnTimer;
        }
        //spawnTimer -= camMath.acceleration * Time.deltaTime * Time.deltaTime;
    }
}
