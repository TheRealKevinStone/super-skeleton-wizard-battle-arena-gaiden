﻿using UnityEngine;
using System.Collections;

public class NetworkManager1 : MonoBehaviour {
    string registeredGameName = "SSWBA:G";
    private float requestLength = 3;
    HostData[] hosts = null;
    bool hasSpawned = false;

    public Transform spawnPoint1;
    public Transform spawnPoint2;


    void Start()
    {
        Application.runInBackground = true;
        Time.timeScale = 0f;
    }

    void OnGUI()
    {

        if (Network.isServer)
        {
            return;
        }

        if (Network.isClient)
        {
            if (GUI.Button(new Rect(25, 25, 200, 30), "Spawn Player") && !hasSpawned)
            {
                SpawnPlayer(false, spawnPoint2);
                hasSpawned = true;
            }
            return;
        }
        if (GUI.Button(new Rect(25, 25, 200, 30), "Start Server"))
        {
            StartServer();
        }

        if (GUI.Button(new Rect(25, 65, 200, 30), "Get Server List"))
        {
            StartCoroutine("GetServerList");
        }


        if (hosts != null && hosts.Length > 0)
        {
            if (GUI.Button(new Rect(25, 105, 200, 30), hosts[0].gameName))
            {
                print(Network.Connect(hosts[0]).ToString());
            }
        }


    }

    public IEnumerator GetServerList()
    {
        MasterServer.RequestHostList(registeredGameName);
        float timeStarted = Time.time;
        float timeEnd = Time.time + requestLength;

        while (Time.time < timeEnd)
        {
            hosts = MasterServer.PollHostList();
            yield return new WaitForEndOfFrame();
        }
    }

    private void StartServer()
    {
        Network.InitializeServer(16, 25002, false);
        MasterServer.RegisterHost(registeredGameName, "Networking SSWBA:G", "VGP142");
    }

    void OnServerInitialized()
    {
        Debug.Log("Server Initialized");
        SpawnPlayer(true, spawnPoint1);
    }

    void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        if (msEvent == MasterServerEvent.RegistrationSucceeded)
        {
            print("Registration Successful");
        }
        else
        {
            print(msEvent.ToString());
        }
    }

    void OnConnectedToServer()
    {
        print("OnConnectedToServer");
    }
    void OnDisconnectedFromServer(NetworkDisconnection dc)
    {
        print("OnDisconnectedFromServer--" + dc.ToString());
    }

    void FailedToConnect(NetworkConnectionError error)
    {
        print("FailedToConnect--" + error.ToString());
    }

    void FailedToConnectToMasterServer(NetworkConnectionError error)
    {
        print("FailedToConnectToMasterServer--" + error.ToString());
    }

    void OnNetworkInstantiate(NetworkMessageInfo info)
    {

        print("OnNetworkInstantiate--" + info.ToString());
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        //remove rpcs and destroy object
    }

    void OnApplicationQuit()
    {
        if (Network.isServer)
        {
            Network.Disconnect();
            MasterServer.UnregisterHost();
        }

    }

    private void SpawnPlayer(bool isHost, Transform spawnPoint)
    {
        if (isHost)
        {
            Network.Instantiate(Resources.Load("Player"), spawnPoint.position, Quaternion.identity, 0);
            Network.Instantiate(Resources.Load("Canvas"), new Vector3(0, 0, 0), Quaternion.identity, 0);
        }
        else
        {
            Network.Instantiate(Resources.Load("Player"), spawnPoint.position, Quaternion.identity, 0);
            Network.Instantiate(Resources.Load("Canvas"), new Vector3(0, 0, 0), Quaternion.identity, 0);

        }
    }
}
