﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour 
{
    // Network
    NetworkView networkView;

    // AI variables
    public enum EnemyState {Patrol, Attack}
    public EnemyState curState;
    public float range;
    //public Transform player;
    public float detectDistance;
    public float iceTimeStep;
    public float frozenDuration;

    // Animator
    private Animator anim;
    private AnimatorStateInfo currentBaseState;
    private AnimatorStateInfo layer2CurrentState;

    static int reloadState = Animator.StringToHash("Layer2.Reload");

    // Movement Variables
    public GameObject waypoint1;
    public GameObject waypoint2;
    private CharacterController enemy;
    public float speed = 3.0f;
    float ySpeed;
    Vector3 velocity;
    bool facingRight;

    // Attack Variables
    public Transform spawnPoint;
    public GameObject spear;
    public float attackCooldown;
    float attackCooldownTimer;
    public float attackTime;
    float attackTimeTimer;
    bool readyToAttack;

    // DROPS
    public GameObject freezeSpellDrop;
    public GameObject fireSpellDrop;
    public GameObject lightningSpellDrop;
    public GameObject heartDrop;

	// Use this for initialization
	void Start () 
    {
        networkView = GetComponent<NetworkView>();
        attackCooldownTimer = 0;
        anim = GetComponent<Animator>();
        enemy = GetComponent<CharacterController>();
        if (anim.layerCount == 2)
            anim.SetLayerWeight(1, 1);
        attackCooldownTimer = attackCooldown;
        //player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate ()
    {
        // Check if enemy is frozen
        frozenDuration -= Time.deltaTime;
        if (frozenDuration > 0)
        {
            iceTimeStep = 0;
        }
        else
        {
            iceTimeStep = 1;
        }

        // Determine enemy facing
        if (facingRight)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        // Raycast to detect player
        //Ray ray = new Ray(transform.position, transform.right);

        //RaycastHit hitInfo;
        //if (Physics.Raycast(ray, out hitInfo, range))
        //{
        //    GameObject objHit = hitInfo.collider.gameObject;
        //    if (objHit.tag == "Player")
        //    {
        //        //isPlayerThere = true;
        //        curState = EnemyState.Attack;
        //    }
        //    else if (objHit.tag != "Player" || objHit == null)
        //    {
        //        //isPlayerThere = false;
        //        curState = EnemyState.Patrol;
        //    }
        //    Debug.Log(objHit.tag);
        //}
        Ray ray = new Ray(transform.position, transform.right);

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, range))
        {
            GameObject objHit = hitInfo.collider.gameObject;
            float playerDistance = hitInfo.distance;
            if (objHit.tag == "Player" && playerDistance <= detectDistance)
            {
                curState = EnemyState.Attack;
            }
            else
            {
                curState = EnemyState.Patrol;
            }
        }
        //Debug.DrawRay(transform.position, player.position, Color.green);
        

        // AI states
        switch (curState)
        {
            case EnemyState.Attack:
                EnemyAttack();
                break;
            case EnemyState.Patrol:
                EnemyPatrol();
                break;
        }

        if (enemy.isGrounded)
        {
            ySpeed = -0.01f;
        }
        else
        {
            ySpeed += Physics.gravity.y * Time.deltaTime * Time.deltaTime;
        }

        enemy.Move(velocity * iceTimeStep);
    }

    void EnemyAttack ()
    {
        if (!readyToAttack){
            attackTimeTimer = attackTime;
        }
        anim.SetFloat("Speed", 0f);
        velocity = new Vector3(0, ySpeed, 0f);
        velocity = transform.rotation * velocity;
        attackCooldownTimer -= Time.deltaTime * iceTimeStep;
        if (attackCooldownTimer <= 0)
        {
            readyToAttack = true;
            anim.SetTrigger("Attack");
            attackTimeTimer -= Time.deltaTime * iceTimeStep;
            if (attackTimeTimer <= 0)
            {
                //Debug.Log("Spear!");
                Instantiate(spear, spawnPoint.position, spawnPoint.rotation);
                attackCooldownTimer = attackCooldown;
                readyToAttack = false;

            }
        }
    }

    void EnemyPatrol()
    {
        anim.SetFloat("Speed", 1f * iceTimeStep);
        //speed = speed * transform.rotation;
        velocity = new Vector3 (speed * Time.deltaTime, ySpeed, 0f);
        velocity = transform.rotation * velocity;
        //Debug.Log("Patrol");
        attackCooldownTimer = attackCooldown;
    }

    void OnTriggerEnter (Collider c)
    {
        if (c.gameObject.tag == "Waypoint 1")
        {
            facingRight = true;
        }
        else if (c.gameObject.tag == "Waypoint 2")
        {
            facingRight = false;
        }
    }

    public void GetRekt()
    {
        int rand = Random.Range(1, 51);
        //Debug.Log(rand);
        if (rand < 2)
        {
            // Drop Ice
            Instantiate(fireSpellDrop, transform.position, new Quaternion());
        }
        if (rand >= 2 && rand < 4)
        {
            // Drop Air
            Instantiate(lightningSpellDrop, transform.position, new Quaternion());

        }
        if (rand >= 4 && rand < 9)
        {
            // Drop Fireball
            Instantiate(freezeSpellDrop, transform.position, new Quaternion());

        }
        else
        {
            Instantiate(heartDrop, transform.position, new Quaternion());
        }
        networkView.RPC("Kill", RPCMode.AllBuffered);
    }

    public void Frozen (float duration)
    {
        frozenDuration = duration;
    }

    [RPC]
    public void Kill()
    {
        Destroy(gameObject);
    }
}
